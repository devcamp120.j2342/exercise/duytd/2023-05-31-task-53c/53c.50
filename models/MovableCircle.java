package models;

import interfaces.Movable;

public class MovableCircle implements Movable{
    private double radius;
    private MovablePoint center;
    
    public MovableCircle(double radius, MovablePoint center) {
        this.radius = radius;
        this.center = center;
    }

    @Override
    public String toString() {
        return "MovableCircle [radius=" + radius + ", center=" + center + "]";
    }
    
    @Override
    public void moveUp() {
        moveUp();
    }
    @Override
    public void moveDown() {
        moveDown();
    }
    public void moveLeft(){
        moveLeft();
    }
    @Override
    public void moveRight() {
        moveRight();
    }
}
