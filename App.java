import models.MovablePoint;

import java.util.concurrent.CountDownLatch;

import models.MovableCircle;
public class App {
    public static void main(String[] args) throws Exception {
        
        MovablePoint point = new MovablePoint(5, 10, 5, 10);

        MovableCircle circle = new MovableCircle(15, point);

        System.out.println(point);
        point.moveUp();
        point.moveDown();
        point.moveLeft();
        point.moveRight();
        System.out.println(point.toString());

        System.out.println(circle);
       
        
        
    }
}
